import com.codeborne.selenide.ElementsCollection;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;


public class GmailTest extends BaseTest {

    @Test
    public void testCommonFlow() {

        open("http://www.gmail.com");

        loginingToGmail(Config.login, Config.password);
        sendMail();
        assertMailArrived();
        assertMailExistInSent();
        searchMail();

    }

    private void searchMail() {
        $(By.name("q")).setValue(mailSubject).pressEnter();
        findMail.filter(visible).shouldHave(texts(mailSubject));

    }

    private void assertMailExistInSent() {
        $(byText("Sent Mail")).click();
        findMail.filter(visible).shouldHave(texts(mailSubject));
    }

    private void sendMail() {
        $(byText("COMPOSE")).click();
        $(By.name("to")).setValue("fffewd1@gmail.com").click();
        $(By.name("subjectbox")).setValue(mailSubject).click();
        $(byText("Send")).click();
    }

    private void loginingToGmail(String login, String password) {
        $("#Email").setValue(login).pressEnter();
        $("#Passwd").setValue(password).pressEnter();
        $(byText("COMPOSE")).shouldBe(visible);
    }

    private void assertMailArrived() {
        $(".vh").shouldHave(text("Your message has been sent."));
    }

    ElementsCollection findMail = $$(".Cp .F.cf.zt");


}
